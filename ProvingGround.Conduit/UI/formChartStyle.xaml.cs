﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Xceed.Wpf.Toolkit;

using Rhino;
using Rhino.Geometry;
using Rhino.DocObjects;
using Rhino.Collections;

using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;


namespace ProvingGround.Conduit.UI
{
	/// <summary>
	/// Interaction logic for formChartStyle.xaml
	/// </summary>
	public partial class formChartStyle : Window
	{

        private bool _baseSet = false;

		public formChartStyle()
		{

			InitializeComponent();

			StackPanel_ColorStack.Children.Add(new ucColor(System.Windows.Media.Color.FromRgb(
				(byte)255,
				(byte)0,
				(byte)0)));
			StackPanel_ColorStack.Children.Add(new ucColor(System.Windows.Media.Color.FromRgb(
				(byte)0,
				(byte)255,
				(byte)0)));
			StackPanel_ColorStack.Children.Add(new ucColor(System.Windows.Media.Color.FromRgb(
				(byte)0,
				(byte)0,
				(byte)255)));

            _baseSet = true;

			ColorViz();

            

		}

		private void ColorViz()
		{
            
            if(_baseSet)
            { 

			StackPanel_ColorViz.Children.Clear();
			int vizCount = 10;
			try 
			{
				vizCount = Convert.ToInt32(TextBox_NumberOfColors.Text);
			}
			catch
			{

			}
			
			int colorCount = StackPanel_ColorStack.Children.Count;


                Interval HR = new Interval(Slider_Hue.LowerValue, Slider_Hue.HigherValue);
                Interval SR = new Interval(Slider_Saturation.LowerValue, Slider_Saturation.HigherValue);
                Interval LR = new Interval(Slider_Brightness.LowerValue, Slider_Brightness.HigherValue);

                Random setRandom = new Random(3);

                for (int i = 0; i < vizCount; i++)
                {

                    System.Windows.Shapes.Rectangle thisRect = new System.Windows.Shapes.Rectangle();

                    thisRect.Width = StackPanel_ColorViz.Width;
                    thisRect.Height = StackPanel_ColorViz.Height / vizCount;

                    System.Windows.Media.Color medColor = ((ucColor)StackPanel_ColorStack.Children[i % colorCount]).ThisColor;

                    Rhino.Display.ColorHSL setHSL = new Rhino.Display.ColorHSL(System.Drawing.Color.FromArgb(
                        Convert.ToInt32(medColor.R),
                        Convert.ToInt32(medColor.G),
                        Convert.ToInt32(medColor.B)));

                    double newHue = OneAdjust(setHSL.H + (i > colorCount - 1 ? HR.ParameterAt(setRandom.NextDouble()) : 0));
                    double newSaturation = OneAdjust(setHSL.S + (i > colorCount - 1 ? SR.ParameterAt(setRandom.NextDouble()) : 0));
                    double newLuminance = OneAdjust(setHSL.L + (i > colorCount - 1 ? LR.ParameterAt(setRandom.NextDouble()) : 0));

                    Rhino.Display.ColorHSL hslC = new Rhino.Display.ColorHSL(newHue, newSaturation, newLuminance);


                    thisRect.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(
                        (byte)hslC.ToArgbColor().R,
                        (byte)hslC.ToArgbColor().G,
                        (byte)hslC.ToArgbColor().B));

                    StackPanel_ColorViz.Children.Add(thisRect);

                }

            }

		}

		private double OneAdjust(double adjust)
		{
			if (adjust > 1)
			{
				return 1;
			}
			else if (adjust < 0)
			{
				return 0;
			}
			else
			{
				return adjust;
			}
		}

		#region event handlers

		private void CustomHandler(object sender, RoutedEventArgs e)
		{
			// Code to handle the event raised from the child
			// Use this line to stop the event bubbling further if you need to
			ColorViz();
			e.Handled = true;
		}

		#endregion

        private void ChangeColorViz(object sender, RoutedEventArgs e)
        {
            ColorViz();
        }

	}
}
